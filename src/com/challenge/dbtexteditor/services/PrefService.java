package com.challenge.dbtexteditor.services;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefService {
	private static final String PREFERENCE_NAME = "DBSyncTextEditorPreferences";
			
	private Context mContext;
	private SharedPreferences mSharedPreferences;
	private static PrefService instance;
	
	public static PrefService getInstance(Context context){
		if(instance == null){
			instance = new PrefService(context);
		}
		return instance;
	}
	
	private PrefService(Context context){
		mContext = context;
		mSharedPreferences = mContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
	}
	
	/**
	 * Sets preference value for key.
	 * @param key
	 * @param value
	 */
	public void setPrefValue(String key, String value){
		mSharedPreferences.edit().putString(key, value).commit();
	}
	
	/**
	 * Returns preference value if key exists 
	 * <br/> else returns default value.
	 * @param key
	 * @param defVal
	 * @return
	 */
	public String getPrefValue(String key, String defVal){
		return mSharedPreferences.getString(key, defVal);
	}
}

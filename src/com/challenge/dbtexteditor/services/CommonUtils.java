package com.challenge.dbtexteditor.services;

import android.util.Log;

/**
 * Contains common methods that can be used across the application.
 * @author toan
 *
 */
public class CommonUtils {
	private static final boolean DEBUG = true;
	
	public static void logError(String tag, String message){
		if(DEBUG){
			Log.e(tag, message);
		}
	}
	
	public static void logInfo(String tag, String message){
		if(DEBUG){
			Log.i(tag, message);
		}
	}
}
